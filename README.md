# Protocol Indicator

This extension shows an indicator for every webpage if HTTP/1, HTTP/2 or HTTP/2+QUIC is used.
Originally the extension [HTTP/2 and SPDY indicator](https://chrome.google.com/webstore/detail/http2-and-spdy-indicator/mpbpobfflnpcgagjijhmgnchggcjblin) showed the current state, but with the deprecation of the `chrome.loadTimes()` API the extension caused errors. This extension is using the new [Navigation Timing 2](https://www.w3.org/TR/navigation-timing-2/) API which was enabled by default since [Chrome 57](https://www.chromestatus.com/feature/5409443269312512).  
Get it from [Chrome Web Store](https://chrome.google.com/webstore/detail/protocol-indicator/fgpjgilbmibmfioicpnnfmlgfpeaoipo).
