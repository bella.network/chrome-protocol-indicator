// Add listener for incoming messages to update icon/title
chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
	const tabId = sender.tab.id
	let title
	let icon

	if (message == 'h2') {
		title = 'HTTP/2'
		icon = 'h2'
	} else if (message == "hq") {
		// Google Servers are sending the header "alt-svc: hq=":443"; ma=2592000; quic=51303433; quic=51303432; quic=51303431; quic=51303339; quic=51303335,quic=":443"; ma=2592000; v="43,42,41,39,35""
		// Number in quic= represents the currently used version of quic: https://github.com/quicwg/base-drafts/wiki/QUIC-Versions
		// As of Chrome 66, only QUIC/39 is supported. chrome://net-internals/#quic --> Supported Versions	QUIC_VERSION_39
		title = 'HTTP/2 + QUIC/39'
		icon = 'hq'
	} else if (message.includes("quic") || message.startsWith("h3")) {
		// Chrome 68+ changed the content of the field which includes the used version
		// Example value: http/2+quic/43
		title = message.replace("+", " + ").replace("http/", "HTTP/").replace("quic/", "QUIC/")
		icon = 'hq'
	} else {
		title = 'HTTP/1'
		icon = 'h1'
	}

	// Set icon and title
	chrome.browserAction.setIcon({ path: 'icons/' + icon + '.svg', tabId })
	chrome.browserAction.setTitle({ tabId, title })
});

// Action when clicking on icon
chrome.browserAction.onClicked.addListener(tab => {
	chrome.browserAction.getTitle({ tabId: tab.id }, result => {
		// chrome://net-internals/#http2 and chrome://net-internals/#quic got removed from Chrome
		// --> The net-internals events viewer and related functionality has been removed. Please use chrome://net-export to save netlogs and the external catapult netlog_viewer to view them.
	})
});

// "tlsInfo" as option is in developement since Jul 2016
// https://bugs.chromium.org/p/chromium/issues/detail?id=628819
//
// TODO if available:
// - Display used TLS-Version, Cipher and other info
//
// Requires webRequest and <all_urls> permission
// Included now to prevent future deactivation due to update
/*
	chrome.webRequest.onCompleted.addListener(function (ret) {
		console.log(ret);
	}, {
		urls: ["<all_urls>"],
		types: ["main_frame"]
	}, ["tlsInfo"]);
*/
